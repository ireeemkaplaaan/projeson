﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using kitapcanavari.Models;

namespace kitapcanavari.DAL
{
    public class kitapcanavariContext : DbContext
    {
        public kitapcanavariContext() : base("kitapcanavariContext") { }
        public DbSet<Uye> Uyes { get; set; }
        public DbSet<Urun> Uruns { get; set; }
        public DbSet<Iletisim> Iletisims { get; set; }
    }
    public class Initializer : IDatabaseInitializer<kitapcanavariContext>
    {
        public void InitializeDatabase(kitapcanavariContext context)
        {
        }
    }
}