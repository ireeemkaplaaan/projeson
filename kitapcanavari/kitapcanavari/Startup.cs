﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(kitapcanavari.Startup))]
namespace kitapcanavari
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
